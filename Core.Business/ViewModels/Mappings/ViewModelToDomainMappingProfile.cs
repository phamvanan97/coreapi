﻿using System;
using AutoMapper;
using Core.Business.ViewModels.Accounts;
using Core.Entity.Abstract;
using Core.Entity.Entities;
using Core.Infrastructure.Http;
using Core.Infrastructure.Security;
using Core.Infrastructure.Utils;

namespace Core.Business.ViewModels.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            //Account
            CreateMap<CreateAccountViewModel, User>()
                .AfterMap((src, dest) =>
                {
                    SetGeneralColsCreate(dest);
                    dest.SecurityStamp = Guid.NewGuid().ToString();
                    dest.PasswordHash = new Encryption().EncryptPassword(src.PassWord, dest.SecurityStamp);
                }).ReverseMap();

            CreateMap<UpdateAccountViewModel, User>()
                .AfterMap((src, dest) =>
                {
                    SetGeneralColsUpdate(dest);
                    if (!Util.IsNull(src.PassWord))
                    {
                        dest.SecurityStamp = Guid.NewGuid().ToString();
                        dest.PasswordHash = new Encryption().EncryptPassword(src.PassWord, dest.SecurityStamp);
                    }
                }).ReverseMap();

            CreateMap<ChangePassWordViewModel, User>()
                .AfterMap((src, dest) =>
                {
                    SetGeneralColsUpdate(dest);
                    if (!Util.IsNull(src.NewPassWord))
                    {
                        dest.SecurityStamp = Guid.NewGuid().ToString();
                        dest.PasswordHash = new Encryption().EncryptPassword(src.NewPassWord, dest.SecurityStamp);
                    }
                }).ReverseMap();
            //CreateMap<VehiclesViewModel, Vehicle>().AfterMap((src, dest) => { if (src.Id > 0) SetGeneralColsUpdate(dest); else SetGeneralColsCreate(dest); }).ReverseMap();
           
            CreateMap<ChangePassWordViewModel, User>()
                .AfterMap((src, dest) =>
                {
                    SetGeneralColsUpdate(dest);
                    if (!Util.IsNull(src.NewPassWord))
                    {
                        dest.SecurityStamp = Guid.NewGuid().ToString();
                        dest.PasswordHash = new Encryption().EncryptPassword(src.NewPassWord, dest.SecurityStamp);
                    }
                }).ReverseMap();            
        }

        public void SetGeneralColsCreate(IEntityBasic data)
        {
            var currentDate = DateTime.Now;
            var currentUserId = HttpContext.CurrentUserId;

            data.Id = 0;
            data.ConcurrencyStamp = Guid.NewGuid().ToString();
            data.CreatedWhen = currentDate;
            data.CreatedBy = currentUserId;
            data.ModifiedWhen = currentDate;
            data.ModifiedBy = currentUserId;
            data.IsEnabled = true;
        }

        public void SetGeneralColsUpdate(IEntityBasic data)
        {
            data.ConcurrencyStamp = Guid.NewGuid().ToString();
            data.ModifiedWhen = DateTime.Now;
            data.ModifiedBy = HttpContext.CurrentUserId;
        }
    }
}
