﻿using System;
using Core.Entity.Entities;
using Core.Entity.Procedures;
using Core.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Core.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public ApplicationContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Connection.Instance.GetConnectionString());
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // --------- ENTITY ---------
            //modelBuilder.Entity<TransportationImage>().ToTable("Beep_TransportationImage");

            // --------- PROC -------------
            modelBuilder.Entity<Proc_SearchEntityByValue>().ToTable(Proc_SearchEntityByValue.ProcName);
            modelBuilder.Entity<Proc_Permission>().ToTable(Proc_Permission.ProcName);
            modelBuilder.Entity<Proc_PermissionDetail>().ToTable(Proc_PermissionDetail.ProcName);
           
        }

        internal object Entry<T>()
        {
            throw new NotImplementedException();
        }
    }
}
