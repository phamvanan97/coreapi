﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Business.Services.Abstract;
using Core.Business.Services.Models;
using Core.Business.ViewModels.User;
using Core.Data.Abstract;
using Core.Entity.Entities;
using Core.Infrastructure.Helper;
using Core.Infrastructure.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Core.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : GeneralController<UserViewModel, User>
    {
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;

        public UserController(
            IUserService userService,
            IAccountService accountService,
            Microsoft.Extensions.Logging.ILogger<dynamic> logger,
            IOptions<AppSettings> optionsAccessor,
            IOptions<JwtIssuerOptions> jwtOptions,
            IUnitOfWork unitOfWork,
            IGeneralService<UserViewModel, User> iGeneralService
            ) : base(logger, optionsAccessor, jwtOptions, unitOfWork, iGeneralService)
        {
            _userService = userService;
            _accountService = accountService;
        }
        // GET: api/<UserController>
        [HttpGet("GetListUser")]
        public async Task<IActionResult> GetListUser(string keySearch, int? pageNum, int? pageSize)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.GetListUserAsync(keySearch, pageNum.GetValueOrDefault(1), pageSize.GetValueOrDefault(10));
        }

        // GET api/<UserController>/5
        [HttpGet("GetDetailUser")]
        public async Task<IActionResult> GetDetailUser(int userId)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.GetDetailUserAsync(userId);
        }

        // POST api/<UserController>
        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] UserViewModel model)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.CreateUserAsync(model);
        }

        // PUT api/<UserController>/5
        [HttpPost("UpdateUser")]
        public async Task<IActionResult> UpdateUser([FromBody] UserViewModel model)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.UpdateUserAsync(model);
        }

        [HttpPost("DeEnableUser")]
        public async Task<IActionResult> DeEnableUser(int userId)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.DeEnabledUserAsync(userId);
        }

        [HttpPost("DeActiveUser")]
        public async Task<IActionResult> DeActiveUser(int userId)
        {
            var currentUserId = GetCurrentUserId();
            var verify = _accountService.VerifyToken(currentUserId);
            if (verify == null) return JsonUtil.Error("Can not get Authorized User");
            return await _userService.DeActiveUserAsync(userId);
        }

        // DELETE api/<UserController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
