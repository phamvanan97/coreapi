﻿using Microsoft.Extensions.DependencyInjection;
using Core.Data.Abstract;
using Core.Business.Services;
using Core.Data.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Core.Api.Core.Sercurity;
using Core.Business.Services.Abstract;

namespace Core.API
{
    public partial class Startup
    {
        private void MappingScopeService(IServiceCollection services)
        {
            //General
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddSingleton<IAuthorizationHandler, PermissionsHandler>();
            //UnitOfWork
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //Services
            services.AddScoped(typeof(IGeneralService), typeof(GeneralService));
            services.AddScoped(typeof(IGeneralService<,>), typeof(GeneralService<,>));
            services.AddScoped(typeof(IGeneralService<,,>), typeof(GeneralService<,,>));
            services.AddScoped(typeof(IGeneralService<,,,>), typeof(GeneralService<,,,>));
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<IAccountService, AccountService>();
            //services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IUserService, UserService>();

            //Repository
            services.AddScoped(typeof(IEntityCRUDRepository<>), typeof(EntityCRUDRepository<>));
            services.AddScoped(typeof(IEntityRRepository<>), typeof(EntityRRepository<>));
            services.AddScoped(typeof(IEntityVPRepository<>), typeof(EntityVPRepository<>));
        }
    }
}
